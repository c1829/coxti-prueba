'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

//Migraciones del usuario, creación de la tabla 'users'

class UserSchema extends Schema {
    up() {
        this.create('users', (table) => {
            table.increments()
            table.string('username', 80).notNullable().unique()
            table.string('name', 80).notNullable()
            table.string('email', 254).notNullable().unique()
            table.string('password', 60).notNullable()
            table.string('iduser', 80).notNullable()
            table.string('cel', 80).notNullable()
            table.string('depart', 80).notNullable()
            table.string('city', 80).notNullable()
            table.string('neigh', 80).notNullable()
            table.string('adress', 80).notNullable()
            table.string('salary', 80).notNullable()
            table.string('other', 80).notNullable()
            table.string('montly', 80).notNullable()
            table.string('financial', 80).notNullable()
            table.timestamps()
        })
    }

    down() {
        this.drop('users')
    }
}

module.exports = UserSchema