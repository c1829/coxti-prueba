'use strict'

//Rutas

const Route = use('Route')

Route.post('Users/create', 'UserController.create') //Ruta del registro de usuarios
Route.post('/salary', 'SalController.salaryCalc') //Ruta del API de salario
Route.post('/login', 'AuthController.login') //Ruta del login