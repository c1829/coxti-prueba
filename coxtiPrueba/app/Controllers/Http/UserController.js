'use strict'
const User = use('App/Services/User');

//Controlador del usuario

class UserController {
    //Recibe la data del usuario y ejecuta el metodo 'createUser()'
    async create({ request, response }) {
        try {
            const data = request.all();
            await User.createUser(data);
            response.ok({ status: true, message: 'Usuario creado satisfactorimante' });
        } catch (error) {
            console.error(error);
            return error;
        }

    }

}

module.exports = UserController