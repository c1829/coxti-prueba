'use strict'

const User = use('App/Models/User')

//Controlador de salario

class SalController {
    //Metodo salaryCalc recibe el dato 'salary' correspondiente al salario del usuario
    async salaryCalc({ request, response }) {
        const { salary } = request.only('salary')
        let salario = `Su salario es de: ${salary}$`;
        let rangoValor = [
            [3000000, 5000000],
            [5000000, 7000000],
            [7000000, 9000000]
        ];
        let rangoRand = rangoValor.sort(() => Math.random() - 0.5);
        let respuesta = "";
        let resF = "";
        let valor = "";

        let rangos = {
            "a": `${rangoRand[0][0]} - ${rangoRand[0][1]}`,
            "b": `${rangoRand[1][0]} - ${rangoRand[1][1]}`,
            "c": `${rangoRand[2][0]} - ${rangoRand[2][1]}`
        };
        console.log('rangos:', rangos)

        for (let i = 0; i < rangoValor.length; i++) {
            if (salary >= rangoRand[i][0] && salary <= rangoRand[i][1]) {
                valor = String.fromCharCode(97 + (i))
                respuesta = `El rango del salario es: ${valor}`
            }
            resF = respuesta
        }
        console.log('respuesta: ', resF)
        console.log('salario: ', salario)

        return response.created({
            salario: salario,
            calc: rangos, //Devuelve uan respuesta con 'salario', correspondiente al salrio del usuario
            res: resF
        })
    }

}

module.exports = SalController