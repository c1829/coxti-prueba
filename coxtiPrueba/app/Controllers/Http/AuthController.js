'use strict'

const User = use('App/Models/User');

//Controlador de la autenticación

class AuthController {
    //Metodo login recibe email y password, devuelve accessToken y user para ser extraidos en el front
    async login({ request, response, auth }) {
        const { email, password } = request.all();
        const accesToken = await auth.attempt(email, password)
        const user = await User.query().where('email', email).fetch()

        return response.ok([accesToken, user])
    }
}

module.exports = AuthController