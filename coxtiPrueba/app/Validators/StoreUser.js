'use strict'

//Clase StoreUser para validar el ingreso de los usuarios

const { formatters } = use('Validator')
class StoreUser {
    get rules() {
        return {
            email: 'required|email|unique:users,email', //email: es requerido, de tipo email y único por usuario
            password: 'required' //password requerida
        }
    }

    get validateAll() {
        return true
    }

    get formatter() {
        return formatters.JsonApi
    }
}

module.exports = StoreUser