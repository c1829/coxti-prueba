'use strict'

//Servicio para crear los usuarios

const UserModel = use('App/Models/User');

class User {
    async createUser(data) {
        await UserModel.create({
            username: data.username,
            name: data.name,
            email: data.email,
            password: data.password,
            iduser: data.iduser,
            cel: data.cel,
            depart: data.depart,
            city: data.city,
            neigh: data.neigh,
            adress: data.adress,
            salary: data.salary,
            other: data.other,
            montly: data.montly,
            financial: data.financial
        })
    }


}

module.exports = new User();