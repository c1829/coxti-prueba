1) Se crea el proyecyo del back-end y el servicio para el usuario que contiene el metodo createUser(), este metodo recibe los datos del usuario y lo crea.
2) Se crea la clase StoreUser para validar los datos del login en el back-end, donde el email es requerido, de tipo email y único por cada usuario, password es requerido.
3) Se crean los controladores para el registro (este controlador contiene el metodo create() para la creación de los usuarios), 
el login (este controlador contiene el metodo login() que valida el ingreso del usuario y almacena sus datos junto con el token para mostrarlos al momento del ingreso) y la API de salario (Este controlador contiene el metodo salaryCalc() que recibe el dato del salario del usuario).
4) Se configuraron las respectivas rutas para cada controlador.
5) Se crea la bd coxti en mySQL y se realizan las migraciones para el usuario en la tabla 'users' y para los tokens en la tabla 'tokens'.
